import  SignUpComponent from '../../../components/SignUpComponent';
import Image from "next/image";
import Link from "next/link";
export default function SignUp() {
    return (
        <div className='w-full h-full flex flex-row bg-white '>
            <div className='w-1/2 flex justify-center items-center'>
                <SignUpComponent />
            </div>
            
            <div className='w-1/2 flex flex-col justify-center items-center'>
                <Image
                  src="/logo.svg"
                  width={80}
                  height={80}
                  alt="logo"
                />
                <span className='font-bold text-xl text-black'>
                    RestGen
                </span>
            </div>
        </div>
        
    );
}

