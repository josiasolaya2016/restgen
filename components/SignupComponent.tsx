"use client";
import { Button, Checkbox, Input, Spacer } from "@nextui-org/react";
import Image from "next/image";
import { useState, FormEvent } from "react";
import { EyeFilledIcon } from "./Icons/EyeFilledIcon";
import { EyeSlashFilledIcon } from "./Icons/EyeSlashFilledIcon";
import React from "react";
import { ServiceSecurity } from "../services/security.service";
import { UserLogin } from "../models/user.model";
import { useRouter } from "next/navigation";

export default function SignUpComponent() {
  const securityService: ServiceSecurity = new ServiceSecurity();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [error, setError] = useState<string | null>(null);
  const [isVisible1, setIsVisible1] = React.useState(false);
  const [isVisible2, setIsVisible2] = React.useState(false);
  const [selected, setSelected] = React.useState(false);
  const router = useRouter();

  async function onSubmit(event: FormEvent<HTMLFormElement>) {
    event.preventDefault();

    const formData = new FormData(event.currentTarget);
    console.log();
    if (formData.get("password") === formData.get("passwordConfirmation")) {
      const login: UserLogin = {
        email: formData.get("email")?.toString(),
        password: formData.get("password")?.toString(),
      };
      const response = await fetch(securityService.url + "auth/signup", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(login),
      });
      securityService.setUser({
        username: "username",
        name: "user",
        access_token: await response.json(),
      });

      if (securityService.user.access_token) {
        router.push("/", { scroll: false });
      }
    } else {
      setError("La contraseñas no coinciden");
      return;
    }
  }

  const toggleVisibility1 = () => setIsVisible1(!isVisible1);
  const toggleVisibility2 = () => setIsVisible2(!isVisible2);

  return (
    <div style={{ ["width"]: "384px" }} className="bg-white text-black">
      <div className="w-full items-center flex flex-row py-3 pl-3 justify-between">
        <div className="flex flex-col">
          <span className="text-2xl font-bold">Sign up</span>
          <span className="text-xs">
            Dou you have an account?{" "}
            <span className="text-blue-900 font-bold">Log in</span>
          </span>
        </div>
        <Image
          alt="user"
          className="object-cover rounded-xl mr-3"
          src="/user-icon.svg"
          width={72}
          height={72}
        />
      </div>
      <form onSubmit={onSubmit} className="flex grid gap-y-3">
        <div className="flex justify-center">
          {error && <div style={{ color: "red" }}>{error}</div>}
        </div>
        <Input variant="bordered" type="text" label="Email" name="email" />
        <Input
          label="Password"
          variant="bordered"
          name="password"
          endContent={
            <button
              className="focus:outline-none"
              type="button"
              onClick={toggleVisibility1}
            >
              {isVisible1 ? (
                <EyeSlashFilledIcon className="text-2xl text-default-400 pointer-events-none" />
              ) : (
                <EyeFilledIcon className="text-2xl text-default-400 pointer-events-none" />
              )}
            </button>
          }
          type={isVisible1 ? "text" : "password"}
        />
        <Input
          label="Password confirmation"
          variant="bordered"
          name="passwordConfirmation"
          endContent={
            <button
              className="focus:outline-none"
              type="button"
              onClick={toggleVisibility2}
            >
              {isVisible2 ? (
                <EyeSlashFilledIcon className="text-2xl text-default-400 pointer-events-none" />
              ) : (
                <EyeFilledIcon className="text-2xl text-default-400 pointer-events-none" />
              )}
            </button>
          }
          type={isVisible2 ? "text" : "password"}
        />
        <Checkbox
          isSelected={selected}
          onChange={() => setSelected(!selected)}
          size="md"
        >
          <span className="text-xs flex my-3">
            I have read and agree to the{" "}
            <span className="text-blue-900 font-bold ml-1">
              Terms of Service
            </span>
          </span>
        </Checkbox>

        <Button
          type="submit"
          color="default"
          className="w-full bg-black text-white"
          disabled={isLoading || !selected}
        >
          {isLoading ? "Loading..." : "Sign up"}
        </Button>
      </form>
    </div>
  );
}
