import { GetStaticPaths, GetStaticProps } from "next";
import Image from "next/image";
import Link from "next/link";
import { useState } from "react";

const NavBar = ({ loginButton, signButton }) => {
  const [nav, setNav] = useState(false);
  return (
    <div className="">
      <nav className="w-full bg-black fixed top-0 left-0 right-0 z-10 ">
        <div className="justify-between lg:max-w-7xl px-4 mx-auto  md:items-center md:flex md:px-8 ">
          <div>
            <div className="flex items-center justify-between py-3 md:py-5 md:block">
              {/* LOGO */}
              <Link href="/" className="flex">
                <Image
                  src="/logo_light.svg"
                  width={30}
                  height={30}
                  alt="logo"
                />
                <h2 className="text-2xl ml-2 text-white font-bold">RestGen</h2>
              </Link>

              {/* HAMBURGER BUTTON FOR MOBILE */}
              <div className="md:hidden">
                <button
                  className="p-2 text-gray-700 rounded-md outline-none focus:border-gray-400"
                  onClick={() => setNav(!nav)}
                >
                  {nav ? (
                    <Image src="/close-white.png" width={30} height={30} alt="logo" />
                  ) : (
                    <Image
                      src="/hamburger-menu-white.png"
                      width={30}
                      height={30}
                      alt="logo"
                      className="focus:border-none active:border-none text-white"
                    />
                  )}
                </button>
              </div>
            </div>
          </div>
          <div className="hidden md:flex">
            <ul className="h-screen md:h-auto items-center justify-center md:flex ">
              <li className="text-base font-bold text-white md:px-6 text-center border-b-2 md:border-b-0  hover:bg-purple-900  border-purple-900  md:hover:text-purple-600 md:hover:bg-transparent">
                <Link href="#recomendations" onClick={() => setNav(!nav)}>
                  Recomendations
                </Link>
              </li>
              <li className="text-base font-bold text-white px-6 text-center  border-b-2 md:border-b-0  hover:bg-purple-600  border-purple-900  md:hover:text-purple-600 md:hover:bg-transparent">
                <Link href="#faqs" onClick={() => setNav(!nav)}>
                  FAQs
                </Link>
              </li>
            </ul>
          </div>
          <div>
            <div
              className={`flex-1 justify-self-center pb-3 mt-8 md:block md:pb-0 z-120 md:mt-0 bg-black ${
                nav ? "p-12 md:p-0 block bg-black" : "hidden"
              }`}
            >
              <ul className="h-screen md:h-auto items-center justify-center md:flex ">
                <li className="md:hidden flex justify-center text-base mb-5 font-bold text-white md:px-6 text-center md:border-b-0  hover:bg-purple-900 md:hover:text-purple-600 md:hover:bg-transparent">
                  <Link href="#recomendations" onClick={() => setNav(!nav)}>
                    Recomendations
                  </Link>
                </li>
                <li className="md:hidden flex justify-center text-base mb-5 font-bold text-white px-6 text-center  md:border-b-0  hover:bg-purple-600 md:hover:text-purple-600 md:hover:bg-transparent">
                  <Link href="#faqs" onClick={() => setNav(!nav)}>
                    FAQs
                  </Link>
                </li>
                <li className="text-base mb-5 font-bold text-white px-6 text-center  md:border-b-0  hover:bg-purple-600 md:hover:text-purple-600 md:hover:bg-transparent">
                  <Link href="login" onClick={() => setNav(!nav)}>
                    { loginButton }
                  </Link>
                </li>
                <li className="text-base mb-5 font-bold text-white px-6 text-center hover:bg-purple-600  border-2 border-white rounded-full md:hover:text-purple-600 md:hover:bg-transparent">
                  <Link href="signup" onClick={() => setNav(!nav)}>
                    { signButton }
                  </Link>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </nav>
    </div>
  );
};

export default NavBar;
